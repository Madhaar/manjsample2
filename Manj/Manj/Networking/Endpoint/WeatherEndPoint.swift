//
//  WeatherEndPoint.swift
//  Manj
//
//  Created by Manjinder Singh on 09/04/2019.
//  Copyright © 2019 Manjinder Singh. All rights reserved.
//

import Foundation

enum NetworkEnvironment {
    case qa
    case production
    case staging
}

public enum WeatherAPI{
    case city(name: String)
}

extension WeatherAPI: EndPointType{
    // Just a way to show you can have different enviorment set here. 
    var environmentBaseURL : String {
        switch NetworkManager.environment {
        case .production: return "https://samples.openweathermap.org/data/2.5/forecast?q="
        case .qa: return "https://samples.openweathermap.org/data/2.5/forecast?q="
        case .staging: return "https://samples.openweathermap.org/data/2.5/forecast?q="
        }
    }
    
    var baseURL: URL {
        guard let url = URL(string: environmentBaseURL) else { fatalError("baseURL could not be configured.")}
        return url
    }
    
    var path: String {
        switch self {
        case .city(let name):
            let str = "\(name)&appid=b6907d289e10d714a6e88b30761fae22"
            return str
        }
    }
    
    var httpMethod: HTTPMethod {
        return .get
    }
    
    var task: HTTPTask {
        switch self {
        case .city(let name):
            return .request
        }
    }
    
    var headers: HTTPHeaders? {
        return nil
    }
    
    
}
