//
//  HTTPMethod.swift
//  Manj
//
//  Created by Manjinder Singh on 09/04/2019.
//  Copyright © 2019 Manjinder Singh. All rights reserved.
//

import Foundation

public enum HTTPMethod : String {
    case get     = "GET"
    case post    = "POST"
    case put     = "PUT"
    case patch   = "PATCH"
    case delete  = "DELETE"
}

