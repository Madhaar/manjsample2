//
//  HTTPTask.swift
//  Manj
//
//  Created by Manjinder Singh on 09/04/2019.
//  Copyright © 2019 Manjinder Singh. All rights reserved.
//

import Foundation
public typealias HTTPHeaders = [String:String]

public enum HTTPTask {
    case request
    
    // case requestParameters, requestParametersAndHeaders, download, upload...etc
    //Can extend if given more time and if requirement changes
}
