//
//  WeatherMain+CoreDataClass.swift
//  Manj
//
//  Created by Manjinder Singh on 23/04/2019.
//  Copyright © 2019 Manjinder Singh. All rights reserved.
//
//

import Foundation
import CoreData

@objc(WeatherMain)
public class WeatherMain: NSManagedObject, Codable {
    
    enum CodingKeys: String, CodingKey {
        case cod = "cod"
        case message
        case cnt
        case list
        case city
    }
    
    // MARK: - Decodable
    required public convenience init(from decoder: Decoder) throws {
        guard let codingUserInfoKeyManagedObjectContext = CodingUserInfoKey.managedObjectContext,
            let managedObjectContext = decoder.userInfo[codingUserInfoKeyManagedObjectContext] as? NSManagedObjectContext,
            let entity = NSEntityDescription.entity(forEntityName: "WeatherMain", in: managedObjectContext) else {
                fatalError("Failed to decode User")
        }
        
        self.init(entity: entity, insertInto: managedObjectContext)
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.cod = try container.decode(String.self, forKey: .cod)
        self.message = try container.decode(Double.self, forKey: .message)
        self.cnt = try container.decode(Int32.self, forKey: .cnt)
        self.list = try container.decode(Set.self, forKey: .list)
        self.city = try container.decode(City.self, forKey: .city)
        
    }
    
    // MARK: - Encodable
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(cod, forKey: .cod)
        try container.encode(message, forKey: .message)
        try container.encode(cnt, forKey: .cnt)
        try container.encode(list, forKey: .list)
        try container.encode(city, forKey: .city)
    }

}
