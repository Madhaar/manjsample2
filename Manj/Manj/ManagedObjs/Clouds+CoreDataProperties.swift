//
//  Clouds+CoreDataProperties.swift
//  Manj
//
//  Created by Manjinder Singh on 23/04/2019.
//  Copyright © 2019 Manjinder Singh. All rights reserved.
//
//

import Foundation
import CoreData


extension Clouds {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Clouds> {
        return NSFetchRequest<Clouds>(entityName: "Clouds")
    }

    @NSManaged public var all: Int32

}
