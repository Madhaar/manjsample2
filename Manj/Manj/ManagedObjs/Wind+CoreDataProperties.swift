//
//  Wind+CoreDataProperties.swift
//  Manj
//
//  Created by Manjinder Singh on 23/04/2019.
//  Copyright © 2019 Manjinder Singh. All rights reserved.
//
//

import Foundation
import CoreData


extension Wind {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Wind> {
        return NSFetchRequest<Wind>(entityName: "Wind")
    }

    @NSManaged public var deg: Double
    @NSManaged public var speed: Double

}
