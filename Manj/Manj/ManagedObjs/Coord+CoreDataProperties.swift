//
//  Coord+CoreDataProperties.swift
//  Manj
//
//  Created by Manjinder Singh on 23/04/2019.
//  Copyright © 2019 Manjinder Singh. All rights reserved.
//
//

import Foundation
import CoreData


extension Coord {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Coord> {
        return NSFetchRequest<Coord>(entityName: "Coord")
    }

    @NSManaged public var lat: Double
    @NSManaged public var lon: Double

}
