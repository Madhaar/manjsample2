//
//  List+CoreDataClass.swift
//  Manj
//
//  Created by Manjinder Singh on 23/04/2019.
//  Copyright © 2019 Manjinder Singh. All rights reserved.
//
//

import Foundation
import CoreData

@objc(List)
public class List: NSManagedObject, Codable {
    
    enum CodingKeys: String, CodingKey {
        case dt = "dt"
        case dt_txt
        case main
        case weather
        case clouds
        case wind
        case sys
    }
    
    // MARK: - Decodable
    required public convenience init(from decoder: Decoder) throws {
        guard let codingUserInfoKeyManagedObjectContext = CodingUserInfoKey.managedObjectContext,
            let managedObjectContext = decoder.userInfo[codingUserInfoKeyManagedObjectContext] as? NSManagedObjectContext,
            let entity = NSEntityDescription.entity(forEntityName: "List", in: managedObjectContext) else {
                fatalError("Failed to decode User")
        }
        
        self.init(entity: entity, insertInto: managedObjectContext)
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.dt = try container.decode(Double.self, forKey: .dt)
        self.dt_txt = try container.decode(String.self, forKey: .dt_txt)
        self.main = try container.decode(Main.self, forKey: .main)
        self.weather = try container.decode(Set<Weather>.self, forKey: .weather)
        self.clouds = try container.decode(Clouds.self, forKey: .clouds)
        self.wind = try container.decode(Wind.self, forKey: .wind)
        self.sys = try container.decode(Sys.self, forKey: .sys)
    }
    
    // MARK: - Encodable
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(dt, forKey: .dt)
        try container.encode(dt_txt, forKey: .dt_txt)
        try container.encode(main, forKey: .main)
        try container.encode(weather, forKey: .weather)
        try container.encode(clouds, forKey: .clouds)
        try container.encode(wind, forKey: .wind)
        try container.encode(sys, forKey: .sys)
    }
}
