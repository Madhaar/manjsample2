//
//  Main+CoreDataClass.swift
//  Manj
//
//  Created by Manjinder Singh on 23/04/2019.
//  Copyright © 2019 Manjinder Singh. All rights reserved.
//
//

import Foundation
import CoreData

@objc(Main)
public class Main: NSManagedObject, Codable {
    
    enum CodingKeys: String, CodingKey {
        case grnd_level = "grnd_level"
        case humidity
        case pressure
        case sea_level
        case temp
        case temp_kf
        case temp_max
        case temp_min
    }
    
    // MARK: - Decodable
    required public convenience init(from decoder: Decoder) throws {
        guard let codingUserInfoKeyManagedObjectContext = CodingUserInfoKey.managedObjectContext,
            let managedObjectContext = decoder.userInfo[codingUserInfoKeyManagedObjectContext] as? NSManagedObjectContext,
            let entity = NSEntityDescription.entity(forEntityName: "Main", in: managedObjectContext) else {
                fatalError("Failed to decode User")
        }
        
        self.init(entity: entity, insertInto: managedObjectContext)
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.grnd_level = try container.decode(Double.self, forKey: .grnd_level)
        self.humidity = try container.decode(Double.self, forKey: .humidity)
        self.pressure = try container.decode(Double.self, forKey: .pressure)
        self.sea_level = try container.decode(Double.self, forKey: .sea_level)
        self.temp = try container.decode(Float.self, forKey: .temp)
        self.temp_kf = try container.decode(Double.self, forKey: .temp_kf)
        self.temp_max = try container.decode(Float.self, forKey: .temp_max)
        self.temp_min = try container.decode(Float.self, forKey: .temp_min)
    }
    
    // MARK: - Encodable
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(grnd_level, forKey: .grnd_level)
        try container.encode(humidity, forKey: .humidity)
        try container.encode(pressure, forKey: .pressure)
        try container.encode(sea_level, forKey: .sea_level)
        try container.encode(temp, forKey: .temp)
        try container.encode(temp_kf, forKey: .temp_kf)
        try container.encode(temp_max, forKey: .temp_max)
        try container.encode(temp_min, forKey: .temp_min)
    }
}
