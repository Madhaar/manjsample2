//
//  Weather+CoreDataClass.swift
//  Manj
//
//  Created by Manjinder Singh on 23/04/2019.
//  Copyright © 2019 Manjinder Singh. All rights reserved.
//
//

import Foundation
import CoreData

@objc(Weather)
public class Weather: NSManagedObject, Codable {
    enum CodingKeys: String, CodingKey {
        case icon = "icon"
        case id
        case main
    }
    
    // MARK: - Decodable
    required public convenience init(from decoder: Decoder) throws {
        guard let codingUserInfoKeyManagedObjectContext = CodingUserInfoKey.managedObjectContext,
            let managedObjectContext = decoder.userInfo[codingUserInfoKeyManagedObjectContext] as? NSManagedObjectContext,
            let entity = NSEntityDescription.entity(forEntityName: "Weather", in: managedObjectContext) else {
                fatalError("Failed to decode User")
        }
        
        self.init(entity: entity, insertInto: managedObjectContext)
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.icon = try container.decode(String.self, forKey: .icon)
        self.id = try container.decode(Int32.self, forKey: .id)
        self.main = try container.decode(String.self, forKey: .main)
    }
    
    // MARK: - Encodable
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(icon, forKey: .icon)
        try container.encode(id, forKey: .id)
        try container.encode(main, forKey: .main)
    }
}
