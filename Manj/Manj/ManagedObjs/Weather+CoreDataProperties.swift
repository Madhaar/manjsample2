//
//  Weather+CoreDataProperties.swift
//  Manj
//
//  Created by Manjinder Singh on 23/04/2019.
//  Copyright © 2019 Manjinder Singh. All rights reserved.
//
//

import Foundation
import CoreData


extension Weather {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Weather> {
        return NSFetchRequest<Weather>(entityName: "Weather")
    }

    @NSManaged public var icon: String?
    @NSManaged public var id: Int32
    @NSManaged public var main: String?

}
