//
//  Sys+CoreDataClass.swift
//  Manj
//
//  Created by Manjinder Singh on 23/04/2019.
//  Copyright © 2019 Manjinder Singh. All rights reserved.
//
//

import Foundation
import CoreData

@objc(Sys)
public class Sys: NSManagedObject, Codable {
    enum CodingKeys: String, CodingKey {
        case pod = "pod"
    }
    
    // MARK: - Decodable
    required public convenience init(from decoder: Decoder) throws {
        guard let codingUserInfoKeyManagedObjectContext = CodingUserInfoKey.managedObjectContext,
            let managedObjectContext = decoder.userInfo[codingUserInfoKeyManagedObjectContext] as? NSManagedObjectContext,
            let entity = NSEntityDescription.entity(forEntityName: "Sys", in: managedObjectContext) else {
                fatalError("Failed to decode User")
        }
        
        self.init(entity: entity, insertInto: managedObjectContext)
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.pod = try container.decode(String.self, forKey: .pod)
    }
    
    // MARK: - Encodable
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(pod, forKey: .pod)
    }
}
