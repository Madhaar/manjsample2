//
//  WeatherMain+CoreDataProperties.swift
//  Manj
//
//  Created by Manjinder Singh on 23/04/2019.
//  Copyright © 2019 Manjinder Singh. All rights reserved.
//
//

import Foundation
import CoreData


extension WeatherMain {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<WeatherMain> {
        return NSFetchRequest<WeatherMain>(entityName: "WeatherMain")
    }

    @NSManaged public var cnt: Int32
    @NSManaged public var cod: String
    @NSManaged public var message: Double
    @NSManaged public var city: City
    @NSManaged public var list:Set<List>

}

// MARK: Generated accessors for list
extension WeatherMain {

    @objc(addListObject:)
    @NSManaged public func addToList(_ value: List)

    @objc(removeListObject:)
    @NSManaged public func removeFromList(_ value: List)

    @objc(addList:)
    @NSManaged public func addToList(_ values: NSSet)

    @objc(removeList:)
    @NSManaged public func removeFromList(_ values: NSSet)

}

extension WeatherMain{

}
