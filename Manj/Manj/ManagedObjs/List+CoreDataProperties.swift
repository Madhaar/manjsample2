//
//  List+CoreDataProperties.swift
//  Manj
//
//  Created by Manjinder Singh on 23/04/2019.
//  Copyright © 2019 Manjinder Singh. All rights reserved.
//
//

import Foundation
import CoreData


extension List {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<List> {
        return NSFetchRequest<List>(entityName: "List")
    }

    @NSManaged public var dt: Double
    @NSManaged public var dt_txt: String
    @NSManaged public var clouds: Clouds
    @NSManaged public var main: Main
    @NSManaged public var sys: Sys
    @NSManaged public var weather: Set<Weather>
    @NSManaged public var weatherMain: WeatherMain
    @NSManaged public var wind: Wind

}

// MARK: Generated accessors for weather
extension List {

    @objc(addWeatherObject:)
    @NSManaged public func addToWeather(_ value: Weather)

    @objc(removeWeatherObject:)
    @NSManaged public func removeFromWeather(_ value: Weather)

    @objc(addWeather:)
    @NSManaged public func addToWeather(_ values: NSSet)

    @objc(removeWeather:)
    @NSManaged public func removeFromWeather(_ values: NSSet)

}
