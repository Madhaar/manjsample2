//
//  Extensions.swift
//  Manj
//
//  Created by Manjinder Singh on 26/04/2019.
//  Copyright © 2019 Manjinder Singh. All rights reserved.
//

import UIKit


public extension CodingUserInfoKey {
    // Helper property to retrieve the context
    static let managedObjectContext = CodingUserInfoKey(rawValue: "managedObjectContext")
}

extension Sequence {
    func groupSort(ascending: Bool = true, byDate dateKey: (Iterator.Element) -> Double) -> [[Iterator.Element]] {
        var categories: [[Iterator.Element]] = []
        for element in self {
            let key = dateKey(element)
            guard let dayIndex = categories.index(where: { $0.contains(where: { Calendar.current.isDate(Date(timeIntervalSince1970: dateKey($0)) , inSameDayAs: Date(timeIntervalSince1970: key)) }) }) else {
                guard let nextIndex = categories.index(where: { $0.contains(where: { Date(timeIntervalSince1970: dateKey($0)).compare(Date(timeIntervalSince1970: key)) == (ascending ? .orderedDescending : .orderedAscending) }) }) else {
                    categories.append([element])
                    continue
                }
                categories.insert([element], at: nextIndex)
                continue
            }
            
            guard let nextIndex = categories[dayIndex].index(where: { Date(timeIntervalSince1970: dateKey($0)).compare(Date(timeIntervalSince1970: key)) == (ascending ? .orderedDescending : .orderedAscending) }) else {
                categories[dayIndex].append(element)
                continue
            }
            categories[dayIndex].insert(element, at: nextIndex)
        }
        return categories
    }
}
var mySpinner : UIView?
extension UIViewController {
    func showSpinner(onView : UIView) {
        let spinnerView = UIView.init(frame: onView.bounds)
        spinnerView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        let ai = UIActivityIndicatorView.init(style: .whiteLarge)
        ai.startAnimating()
        ai.center = spinnerView.center
        
        DispatchQueue.main.async {
            spinnerView.addSubview(ai)
            onView.addSubview(spinnerView)
        }
        mySpinner = spinnerView
    }
    
    func removeSpinner() {
        DispatchQueue.main.async {
            mySpinner?.removeFromSuperview()
            mySpinner = nil
        }
    }
}
