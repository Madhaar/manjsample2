//
//  ViewController.swift
//  Manj
//
//  Created by Manjinder Singh on 10/04/2019.
//  Copyright © 2019 Manjinder Singh. All rights reserved.
//

import UIKit

class ViewController: UIViewController, WeatherDelegate {
    
    let cellIdentifier = "WeatherCell"
    var dataManager: DataManagerProtocol
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var myCollectionView: UICollectionView!
    
    var weatherData = [[List]]()
    
    init(dataManager: DataManagerProtocol) {
        self.dataManager = dataManager
        super.init(nibName: "ViewController", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        myCollectionView.register(UINib(nibName:"WeatherCell", bundle: nil), forCellWithReuseIdentifier:cellIdentifier)
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection         = .horizontal
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing      = 0
        myCollectionView.setCollectionViewLayout(layout, animated: false)
        myCollectionView.isPagingEnabled = true
        myCollectionView.delegate = self
        myCollectionView.dataSource = self
        loadData()
        
    }
    
    func loadData(){
         showSpinner(onView: self.view)
        self.dataManager.getData(offlineCompletionHanlder: { [weak self] (weatherMain) in
         
            self?.removeSpinner()
            if weatherMain != nil{
                self?.weatherData =   weatherMain!.list.groupSort(byDate: { (list) -> Double in
                    return list.dt
                })
                DispatchQueue.main.async {
                    self?.titleLabel.text = weatherMain!.city.name
                    self?.myCollectionView.reloadData()
                }
            }
        }) { [weak self] (weatherMain) in
            if weatherMain != nil{
                self?.weatherData =   weatherMain!.list.groupSort(byDate: { (list) -> Double in
                    return list.dt
                })
                DispatchQueue.main.async {
                    self?.titleLabel.text = weatherMain!.city.name
                    self?.myCollectionView.reloadData()
                }
        }
    }
    }
}

extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return weatherData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: WeatherCell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! WeatherCell
        cell.loadData(with: weatherData[indexPath.row])
        cell.delegate = self
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.size.width, height: collectionView.frame.size.height)
    }
    
    func selectedWeatherData(list: List) {
        self.navigationController?.pushViewController(DetailsViewController(listData: ListViewModel(with: list)), animated: true)
    }
}
