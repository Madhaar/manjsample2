//
//  TableCell.swift
//  Manj
//
//  Created by Manjinder Singh on 14/04/2019.
//  Copyright © 2019 Manjinder Singh. All rights reserved.
//

import UIKit

class TableCell: UITableViewCell {
    
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var upLabel: UILabel!
    @IBOutlet weak var lowLabel: UILabel!
    @IBOutlet weak var wind: UILabel!
    @IBOutlet weak var windDeg: UILabel!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.backgroundColor = .clear
    }
    
    func loadData(with list: List ){
    
        let listMV = ListViewModel(with: list)
        timeLabel.text = listMV.time
        tempLabel.text = listMV.temp
        upLabel.text = listMV.highestTemp
        lowLabel.text = listMV.lowestTemp
        wind.text = listMV.speed
        windDeg.text = listMV.windDeg
    }
}
