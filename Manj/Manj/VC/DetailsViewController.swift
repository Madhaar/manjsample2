//
//  DetailsViewController.swift
//  Manj
//
//  Created by Manjinder Singh on 28/04/2019.
//  Copyright © 2019 Manjinder Singh. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController {

    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var temp_maxLabel: UILabel!
    @IBOutlet weak var temp_minLabel: UILabel!
    @IBOutlet weak var pressureLabel: UILabel!
    @IBOutlet weak var seaLavelLabel: UILabel!
    @IBOutlet weak var grndLavelLabel: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!
    
    @IBOutlet weak var cloudConditionLabel: UILabel!
    @IBOutlet weak var speedLabel: UILabel!
    @IBOutlet weak var degLabel: UILabel!
    
    var list: ListViewModel
    
    // MARK: - Load View
    init(listData: ListViewModel) {
        self.list = listData
        super.init(nibName: "DetailsViewController", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dateLabel.text = list.time
        tempLabel.text = "Temprature: \(list.temp)"
        temp_minLabel.text = "Temp Min: \(list.lowestTemp)"
        temp_maxLabel.text = "Temp Max: \(list.highestTemp)"
        pressureLabel.text = list.pressure
        seaLavelLabel.text = list.sea_level
        grndLavelLabel.text = list.grnd_Level
        humidityLabel.text = list.humidity

        cloudConditionLabel.text = list.cloudCondition
        speedLabel.text = list.speed
        degLabel.text = "Wind Deg: \(list.windDeg)"
        
    }
    
    @IBAction func backButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
