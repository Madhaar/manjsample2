//
//  WeatherCell.swift
//  Manj
//
//  Created by Manjinder Singh on 14/04/2019.
//  Copyright © 2019 Manjinder Singh. All rights reserved.
//

import UIKit

protocol WeatherDelegate: class {

    func selectedWeatherData(list: List)
}

class WeatherCell: UICollectionViewCell{
    
    let TableIdentifier = "TableCell"
   weak var delegate: WeatherDelegate?
    
    @IBOutlet weak var myTableView: UITableView!
    @IBOutlet weak var dayLabel: UILabel!
    
    var weatherData = [List]()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func loadData(with weatherData: [List]) {
        
        guard weatherData.count > 0 else{
            return;
        }
        myTableView.register(UINib(nibName: TableIdentifier, bundle: nil), forCellReuseIdentifier: TableIdentifier)
        self.weatherData = weatherData
        let components =  weatherData.first!.dt_txt.components(separatedBy:  " ")
        if components.count > 1{
            dayLabel.text = weatherData.first!.dt_txt.components(separatedBy: " ").first!
        }
        myTableView.tableFooterView = UIView()
    }
}

extension WeatherCell: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return weatherData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: TableCell = tableView.dequeueReusableCell(withIdentifier: TableIdentifier) as! TableCell
        cell.selectionStyle = .none
        cell.loadData(with: weatherData[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
 
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.delegate?.selectedWeatherData(list: weatherData[indexPath.row])
    }
}
