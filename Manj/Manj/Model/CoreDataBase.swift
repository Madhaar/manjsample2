//
//  CoreDataBase.swift
//  Manj
//
//  Created by Manjinder Singh on 22/04/2019.
//  Copyright © 2019 Manjinder Singh. All rights reserved.
//


import UIKit
import CoreData

let sharedCoreDataBase = CoreDataBase()

class CoreDataBase {
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "ManjSample")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                DispatchQueue.main.async {
                    let alertController =  UIAlertController(title: "Error", message: "Database error", preferredStyle: .alert)
                    var okAction = UIAlertAction(title: "Ok", style: .default) {
                        UIAlertAction in
                        NSLog("OK Pressed")
                    }
                    alertController.addAction(okAction)
                    (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController?.present(alertController, animated: true, completion: nil)
                }
            }else{
                
                container.viewContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
                container.viewContext.automaticallyMergesChangesFromParent = true
                
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                let alertController =  UIAlertController(title: "Error", message: "\(nserror.userInfo)", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "Ok", style: .default) {
                    UIAlertAction in
                    NSLog("OK Pressed")
                }
                alertController.addAction(okAction)
                (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController?.present(alertController, animated: true, completion: nil)
                //fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
}

