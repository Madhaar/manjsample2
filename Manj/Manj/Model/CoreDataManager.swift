//
//  CoreDataManager.swift
//  Manj
//
//  Created by Manjinder Singh on 22/04/2019.
//  Copyright © 2019 Manjinder Singh. All rights reserved.
//

import Foundation
import CoreData

class CoreDataManager: ModelProtocol {
  
    let context: NSManagedObjectContext
    let persistentContainer: NSPersistentContainer!
    let persistentContainerQueue = OperationQueue();
    
    //MARK: Init with dependency
    init(container: NSPersistentContainer) {
        self.persistentContainer = container
        self.persistentContainer.viewContext.automaticallyMergesChangesFromParent = true
        persistentContainerQueue.maxConcurrentOperationCount = 1
        context  =  self.persistentContainer.viewContext
    }
    
    convenience init() {
        //Use the default container for production environment
        self.init(container: sharedCoreDataBase.persistentContainer)
    }
    
    func saveContext(context: NSManagedObjectContext, completion: @escaping () -> Void) {
        do {
            try context.save()
            completion()
        } catch {
            fatalError("Failure to save context: \(error)")
            // completion()
        }
    }
}

extension CoreDataManager{
    func saveData(_ weatherData: Data, completionHandler:@escaping () -> Void){
        self.persistentContainer.performBackgroundTask({ (backgroundContext) in
            backgroundContext.mergePolicy = NSMergePolicy(merge: NSMergePolicyType.overwriteMergePolicyType)
            
            do {
                guard let codingUserInfoKeyManagedObjectContext = CodingUserInfoKey.managedObjectContext else {
                    fatalError("Failed to retrieve managed object context")
                }
                
                // Clear storage and save managed object instance
                   self.clearStorage()
                
                // Parse JSON data
                let managedObjectContext = self.persistentContainer.viewContext
                let decoder = JSONDecoder()
                decoder.userInfo[codingUserInfoKeyManagedObjectContext] = managedObjectContext
                _ = try decoder.decode(WeatherMain.self, from: weatherData)
                try managedObjectContext.save()
                 completionHandler()
                
            } catch let error {
                print(error)
                 completionHandler()
            }
        })
    }
    
    func clearStorage() {
        let managedObjectContext = persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "WeatherMain")
        let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        do {
            try managedObjectContext.execute(batchDeleteRequest)
        } catch let error as NSError {
            print(error)
        }
    }
    
    func getWeatherData() -> WeatherMain? {
        let managedObjectContext = persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<WeatherMain>(entityName: "WeatherMain")
        do {
            let weatherMain = try managedObjectContext.fetch(fetchRequest)
            return weatherMain.first
        } catch let error {
            print(error)
            return nil
        }
    }
}
