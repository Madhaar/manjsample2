//
//  DataManager.swift
//  Manj
//
//  Created by Manjinder Singh on 06/04/2019.
//  Copyright © 2019 Manjinder Singh. All rights reserved.
//

import Foundation

//let sharedDataManager = DataManager(sharedCoreDataManager)
protocol DataManagerProtocol{
    func getData(offlineCompletionHanlder:@escaping (WeatherMain?) -> Void, onlineCompletionHanlder:@escaping (WeatherMain?) -> Void)
}

class DataManager<NP: NetworkProtocol, MP: ModelProtocol>: DataManagerProtocol {
    
    let networkManager  : NP
    let modelManager    : MP
    
    init(_ networkManager: NP, modelManager: MP) {
        self.networkManager = networkManager
        self.modelManager   = modelManager
    }
}

//Post
extension DataManager{
    
    func getData(offlineCompletionHanlder:@escaping (WeatherMain?) -> Void, onlineCompletionHanlder:@escaping (WeatherMain?) -> Void){
        
        offlineCompletionHanlder(self.modelManager.getWeatherData())
        
        self.networkManager.getData(WeatherAPI.city(name: "London")) {[weak self](data: Data?, error) in
            if data != nil{
                self?.modelManager.saveData(data!, completionHandler: {
                    onlineCompletionHanlder(self?.modelManager.getWeatherData())
                })
            }else{
                onlineCompletionHanlder(nil)
            }
        }
    }
}
