//
//  ModelProtocol.swift
//  Manj
//
//  Created by Manjinder Singh on 22/04/2019.
//  Copyright © 2019 Manjinder Singh. All rights reserved.
//

import Foundation

protocol ModelProtocol {
    func saveData(_ weatherData: Data, completionHandler:@escaping () -> Void)
    func getWeatherData() -> WeatherMain?
}
