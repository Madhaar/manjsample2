//
//  ManjTests.swift
//  ManjTests
//
//  Created by Manjinder Singh on 10/04/2019.
//  Copyright © 2019 Manjinder Singh. All rights reserved.
//

import XCTest
@testable import Manj

class ManjTests: XCTestCase {

    let session = MockURLSession()
    var networkManager: NetworkManager?
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        continueAfterFailure = false
        networkManager = NetworkManager(session)
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testDataIsReturned(){
        
        session.nextData = expectedWeatherData
        var actualData: Any?
        networkManager?.getData(WeatherAPI.city(name: "London"), callback: { (data, error) in
            actualData = weather
            XCTAssertNotNil(actualData)
        })
    }
    
    
    func testResumeCalled(){
        let dataTask  = MockURLSessionDataTask()
        session.nextDataTask = dataTask
        networkManager?.getData(WeatherAPI.city(name: "London"), callback: { ( data, error) in
            
        })
        
        XCTAssert(dataTask.resumeWasCalled)
    }
    
    func testListViewModel(){
        do{
            let data = try JSONDecoder().decode(WeatherMain.self, from: expectedWeatherData!)
            let listVM = ListViewModel(with: data.list.first!)
            XCTAssertNotNil(listVM)
            XCTAssertTrue(listVM.temp == "286.6°")
            XCTAssertTrue(listVM.highestTemp == "286.6°")
            XCTAssertTrue(listVM.lowestTemp == "281.5°")
            XCTAssertTrue(listVM.speed == "Speed 1.81")
            XCTAssertTrue(listVM.windDeg == "247.5°")
            
        }catch{
            XCTFail("Post Json is not parsed")
        }
    }
}
